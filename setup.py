from distutils.core import setup

setup(
	name='UrpSpider',
	version='1.0-alpha',
	packages=['', 'URPInfoSpider', 'URPInfoSpider.mail'],
	url='james.letec.top',
	license='Apache License 2.0',
	author='James',
	author_email='zhengbaole_1996@163.com',
	description='A spider for URP of HEBUST.'
)
