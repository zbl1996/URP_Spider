# -*- coding:utf-8 -*-
"""
发送邮件
@author:James
Created on:18-4-12 12:31
"""
import smtplib
from email.header import Header
from email.mime.text import MIMEText

from URPInfoSpider.mail import conf_mail


class Mail(object):
	def __init__(self):
		self.conf = conf_mail
		self.mail_sender = self.conf.SENDER
		self.mail_host = self.conf.HOST
		self.mail_user = self.conf.USERNAME
		self.mail_pwd = self.conf.PWD
		self.come = self.conf.FROM
		self.to = self.conf.TO
		self.title = self.conf.TITLE
		self.text = self.conf.TEXT

	def sendMail(self):
		sender = self.mail_sender
		receivers = [self.conf.RECEIVER]

		message = MIMEText(self.text, 'plain', 'utf-8')
		message['From'] = Header(self.mail_sender, 'utf-8')
		message['To'] = Header(self.mail_sender, 'utf-8')
		message['Subject'] = Header(self.title, 'utf-8')

		smtpObj = smtplib.SMTP()
		smtpObj.connect(self.mail_host, 25)
		smtpObj.login(self.mail_user, self.mail_pwd)
		smtpObj.sendmail(sender, receivers, message.as_string())
		print("Email send successfully.")
