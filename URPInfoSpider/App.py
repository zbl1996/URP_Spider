# -*- coding:utf-8 -*-
"""
科大含理工
二合一

@author:James
Created on:18-5-15 17:55
"""

from URPInfoSpider.application import InfoMain
from URPInfoSpider.application_lg import InfoMainLG

if __name__ == '__main__':
	appLG = InfoMainLG()
	app = InfoMain()
	appLG.autorun()
	app.autorun()
